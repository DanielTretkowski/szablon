<?php

namespace Sda\Szablon\Controller;

use Sda\Szablon\Auto\AutoRepository;
use Sda\Szablon\Request\Request;
use Sda\Szablon\Tools\Tools;

class ApiController {
    
   private $repo;
   private $request;
   

    public function __construct(AutoRepository $repo, Request $request) {
        $this->repo = $repo;
        $this->request = $request;
    }
        public function run(){
            
             $action = $this->request->getParamFromGet('action', 'index');
             
              switch ($action){
                     case 'index':
                            $this->repo->getAuto();
                            $this->repo->getAllAutos();
                            Tools::preFormat($_SERVER, 'v');
                        break;
                    
                     default :
                        echo '404';
                        break;
                }
        }
    
}
