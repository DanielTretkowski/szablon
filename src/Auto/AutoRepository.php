<?php

namespace Sda\Szablon\Auto;

use Doctrine\DBAL\Connection;

class AutoRepository {
  
      private $dbh;

    public function __construct(Connection $dbh) {
        $this->dbh = $dbh;
    }
    
    
    public function getAuto($id = 2){
        
        $sth = $this->dbh->prepare('SELECT * FROM `samochody` WHERE `id` = :id');
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();

        $loginData = $sth->fetch();
        
        var_dump($loginData);
    }
    
    public function getAllAutos(){
         $sth = $this->dbh->prepare('SELECT * FROM `samochody` WHERE `id` > 1');
         $sth->execute();
         $loginData = $sth->fetchAll();
         
         foreach ($loginData as $row){
             echo '</br>'.$row['id'];
             echo $row['Name'];
             echo $row['Place'] .'</br>';
         }
    }
}
