<?php

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Szablon\Controller\ApiController;
use Sda\Szablon\Config\Config;
use Sda\Szablon\Auto\AutoRepository;
use Sda\Szablon\Request\Request;

require_once __DIR__ . '/../vendor/autoload.php';


$config = new Configuration();
$request = new Request();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$repo = new AutoRepository($dbh);

$app = new ApiController($repo, $request);
$app->run();