<?php
namespace Sda\Szablon\Tools;

abstract class Tools {

    public static function preFormat ($data, $type = 'v'){
        
        echo '<pre>';
        
        switch ($type){
            case 'v':
                var_dump($data);
                break;
            case 'p':
                print_r($data);
                break;
            default :
                var_dump($data);
                break;
        }
        echo '</pre>';
    }
    
}
